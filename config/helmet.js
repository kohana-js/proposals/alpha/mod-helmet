module.exports = {
  CSP: {
    'script-src': ["'self'", "'unsafe-inline'", 'blob:'],
    'connect-src': ["'self'", "'unsafe-inline'"],
    'font-src': ["'self'", 'data:'],
    'style-src': ["'self'", "'unsafe-inline'"],
    'img-src': ["'self'", "'unsafe-inline'", 'data:'],
    'script-src-attr': ["'self'", "'unsafe-inline'"],
    'media-src': ["'self'", 'blob:'],
  },
};
